<?php

/**
 * @file
 * Non-Daemon Daemon API functionality including an administrative interface.
 */

/**
 * Implements hook_libraries_info().
 *
 * Used to describe the PHP-Daemon library including which files to load.
 */
function drushd_libraries_info() {
  $items = array();

  $items['PHP-Daemon'] = array(
    'name' => 'PHP Daemon',
    'vendor_url' => 'https://github.com/shaneharter/PHP-Daemon',
    'download_url' => 'https://github.com/shaneharter/PHP-Daemon',
    // Unfortunately I had to hardcode the version here since there is no way
    // to programatically determine the version of the PHP_Daemon. I can't even
    // use regular expressions on the README :(.
    'version' => '2.0',
    'files' => array(
      'php' => array(
        'Core/Daemon.php',
        'Core/IPlugin.php',
        'Core/ITask.php',
        'Core/IWorker.php',

        'Core/Plugin/ProcessManager.php',

        'Core/Lock/Lock.php',
        'Core/Lock/File.php',

        'Core/error_handlers.php',
      ),
    ),
  );

  return $items;
}

/**
 * Implements hook_daemon_api_info().
 *
 * Registers our Daemon with the Drush Daemon API.
 */
function drushd_daemon_api_info() {
  $daemon = array();

  // This is the default Daemon & basically just tells you to create your
  // own daemon.
  $daemon['drush_daemon'] = array(
    // The machine name of the daemon (same as key above).
    'machine_name' => 'drush_daemon',
    // A human-readable name for your daemon.
    'name' => 'Drush Daemon',
    // This module (ie: the module implementing the daemon).
    'module' => 'drushd',
    // The class extending DaemonApi_DrupalDaemon and implementing your
    // daemon-specific functionality. This class should be in a [classname].inc
    // file in your modules base directory.
    'class' => 'DrushDaemon',
    // OPTIONAL: Define this if your module doesn't follow the rule mentioned
    // in the above comment. The name and path to the file containing the
    // daemon class assuming your module folder as the root.
    'class_file' => 'daemon_classes/DrushDaemon.inc',
  );

  // This is an example daemon which just sleeps for random amounts of time.
  $daemon['example'] = array(
    // The machine name of the daemon (same as key above).
    'machine_name' => 'example',
    // A human-readable name for your daemon.
    'name' => 'Example Daemon',
    // This module (ie: the module implementing the daemon).
    'module' => 'drushd',
    // The class extending DrupalDaemon and implementing your daemon-specific
    // functionality. This class should be in a [classname].inc file in your
    // modules base directory.
    'class' => 'DrushDaemonExample',
    // OPTIONAL: Define this if your module doesn't follow the rule mentioned
    // in the above comment. The name and path to the file containing the
    // daemon class assuming your module folder as the root.
    'class_file' => 'daemon_classes/DrushDaemonExample.inc',
  );

  return $daemon;
}
